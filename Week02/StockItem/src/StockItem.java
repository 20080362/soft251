/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 20080362
 */
public class StockItem {
    protected String name;
    protected Integer qty;
    protected Double sellingPrice = 1000000.00;
    protected Double costPrrice = 1000000.00;
    public StockItem(String name) {
        this.name = name;
    }

    public StockItem(String name, Integer qty) {
        this.name = name;
        this.qty = qty;
    }
    
}
