/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unidemo;

/**
 *
 * @author 20080362
 */
class Lecturer extends UniPeople implements ITeach {
        Course course;
    public Lecturer(Integer id, String name) {
        super(id, name);
    }
    
    public void setCourseWork(String a){
        this.course.coursework = a;
    }
    public void teach(){
        System.out.println(this.course.code);
    }
}
