/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unidemo;

/**
 *
 * @author 20080362
 */
public abstract class UniPeople {
    Course c;
    Integer id;
    String name;

    public UniPeople(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Course getC() {
        return c;
    }

    public void setC(Course c) {
        this.c = c;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }   
}
